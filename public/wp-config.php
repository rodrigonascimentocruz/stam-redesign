<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'stam' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'intest' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'stam_db' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'FQf6C}m6:I>RO<~m_n+%Gxml?GVM[j&dE/g4K1j62;)5O+_Y@U}2$bAm1pHhj(oK' );
define( 'SECURE_AUTH_KEY',  'CAb`K#| 0i0hNrxlfnH[VWcE?$JQW!Zy^w3UuSW05Z @*Q)=ce;LqpZN|}_:U7cA' );
define( 'LOGGED_IN_KEY',    'k2yo6Iy[]zGq-??C/Hbrf@*_D!l#/741[1wpL:XCZHVeG$gHs{RnX5E c%ndv>L&' );
define( 'NONCE_KEY',        'W#:pSZNq/_5s+Gz<o{}-y+^j825`x>?H(ZGB*< [-um5{:M2XOz.mLhZLe< ;^24' );
define( 'AUTH_SALT',        '-xt>7El`v4jNyP8Wd<O/~nPQPch` Pop y+h>#W<rHy@;!!-H1VC$5z8DHSvQo  ' );
define( 'SECURE_AUTH_SALT', 'BWlH8WU^$Z*cm}>FrDx/uW.z9TPnyvA~AwEq4Qa y5ABk13t(;-r-Z!Ti7gkq3]J' );
define( 'LOGGED_IN_SALT',   'zk$ZNV<T,)43VRgARZ+W(Qxw([]~;pJ]qWk[}[dB?&F)ZhRj`+2YS0D/Z3R<t%Y0' );
define( 'NONCE_SALT',       'HF`%J3IxXI8nZPKFRf?2-M)I;Y*BCI0[*A{DXO`}y,K@(u V u+3Vpg-DR;i_s+]' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Adicione valores personalizados entre esta linha até "Isto é tudo". */



/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
